import unittest
from app import sumar


class SumaTest(unittest.TestCase):
    def test_sumar(self):
        self.assertEqual(sumar(2,2), 4)

if __name__ == "__main__":
    unittest.main()
